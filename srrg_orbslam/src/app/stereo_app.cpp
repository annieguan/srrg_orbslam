#include <iostream>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <chrono>

#include <opencv2/core/core.hpp>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

using namespace std;

void LoadImages(const string &strPathToSequence,
                vector<string> &vstrImageLeft,
                vector<string> &vstrImageRight,
                vector<double> &vTimestamps);

const char* banner [] = {
  "stereo_orbslam",
  "",
  "usage: stereo_app -v <vocabulary> -s <settings> -data <path/to/dataset> -o <traj-kitti-format>",
  "-data     <string>        path to dataset containing image_0 and image_1 folders and times.txt file",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-ng       <flag>          no gui",
  "-o        <string>        otuput trajectory in KITTI format",
  "-h        <flag>          this help",
  0
};


int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }
  int c = 1;

  //params
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  std::string dataset_filename = "";
  std::string output_filename = "orb_slam_output-kitti.txt";
  bool use_gui = true;

  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-data")) {
      c++;
      dataset_filename = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      output_filename = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    }
    c++;
  } 

  std::cerr<<"dataset_filename     (-data):       "<< dataset_filename << std::endl;
  std::cerr<<"vocabulary           (-v):          "<< vocabulary_filename << std::endl;
  std::cerr<<"settings             (-s):          "<< settings_filename << std::endl;
  std::cerr<<"output               (-o):          "<< output_filename << std::endl;
  std::cerr<<"use_gui              (-ng):         "<< use_gui << std::endl;  
  
  if(vocabulary_filename.empty() ||
     settings_filename.empty()   ||
     dataset_filename.empty()) {
    return 1;
  }

  
  // Retrieve paths to images
  vector<string> vstrImageLeft;
  vector<string> vstrImageRight;
  vector<double> vTimestamps;
  LoadImages(dataset_filename, vstrImageLeft, vstrImageRight, vTimestamps);

  const int nImages = vstrImageLeft.size();

  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  ORB_SLAM2::System SLAM(vocabulary_filename,settings_filename,ORB_SLAM2::System::STEREO,use_gui);

  // Vector for tracking time statistics
  vector<float> vTimesTrack;
  vTimesTrack.resize(nImages);

  cout << endl << "-------" << endl;
  cout << "Start processing sequence ..." << endl;
  cout << "Images in the sequence: " << nImages << endl << endl;   

  // Main loop
  cv::Mat imLeft, imRight;
  for(int ni=0; ni<nImages; ni++)
    {
      // Read left and right images from file
      imLeft = cv::imread(vstrImageLeft[ni],CV_LOAD_IMAGE_UNCHANGED);
      imRight = cv::imread(vstrImageRight[ni],CV_LOAD_IMAGE_UNCHANGED);
      double tframe = vTimestamps[ni];

      if(imLeft.empty())
        {
          cerr << endl << "Failed to load image at: "
               << string(vstrImageLeft[ni]) << endl;
          return 1;
        }

      std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

      // Pass the images to the SLAM system
      SLAM.TrackStereo(imLeft,imRight,tframe);

      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

      double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

      vTimesTrack[ni]=ttrack;

      // Wait to load the next frame
      double T=0;
      if(ni<nImages-1)
        T = vTimestamps[ni+1]-tframe;
      else if(ni>0)
        T = tframe-vTimestamps[ni-1];

      if(ttrack<T)
        usleep((T-ttrack)*1e6);
    }

  // Stop all threads
  SLAM.Shutdown();

  // Tracking time statistics
  sort(vTimesTrack.begin(),vTimesTrack.end());
  float totaltime = 0;
  for(int ni=0; ni<nImages; ni++)
    {
      totaltime+=vTimesTrack[ni];
    }
  cout << "-------" << endl << endl;
  cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
  cout << "mean tracking time: " << totaltime/nImages << endl;

  // Save camera trajectory
  SLAM.SaveTrajectoryKITTI(output_filename);
  std::cerr << "Saved output Trajetory in " << output_filename << std::endl;
  
  return 0;
}

void LoadImages(const string &strPathToSequence, vector<string> &vstrImageLeft,
                vector<string> &vstrImageRight, vector<double> &vTimestamps)
{
  ifstream fTimes;
  string strPathTimeFile = strPathToSequence + "/times.txt";
  fTimes.open(strPathTimeFile.c_str());
  while(!fTimes.eof())
    {
      string s;
      getline(fTimes,s);
      if(!s.empty())
        {
          stringstream ss;
          ss << s;
          double t;
          ss >> t;
          vTimestamps.push_back(t);
        }
    }

  string strPrefixLeft = strPathToSequence + "/image_0/";
  string strPrefixRight = strPathToSequence + "/image_1/";

  const int nTimes = vTimestamps.size();
  vstrImageLeft.resize(nTimes);
  vstrImageRight.resize(nTimes);

  for(int i=0; i<nTimes; i++)
    {
      stringstream ss;
      ss << setfill('0') << setw(6) << i;
      vstrImageLeft[i] = strPrefixLeft + ss.str() + ".png";
      vstrImageRight[i] = strPrefixRight + ss.str() + ".png";
    }
}
