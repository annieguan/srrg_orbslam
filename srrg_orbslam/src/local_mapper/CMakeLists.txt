add_library(srrg_orbslam_local_mapper_library SHARED
  LocalMapping.cc
)

target_link_libraries(srrg_orbslam_local_mapper_library
  ${OpenCV_LIBS}
)
