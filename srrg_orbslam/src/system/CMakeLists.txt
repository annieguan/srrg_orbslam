add_library(srrg_orbslam_system_library SHARED
  Initializer.cc
  System.cc
)

target_link_libraries(srrg_orbslam_system_library
  srrg_orbslam_types_library
  srrg_orbslam_utils_library
  srrg_orbslam_solver_library
  srrg_orbslam_detector_library
  srrg_orbslam_optimizer_library
  srrg_orbslam_local_mapper_library
  srrg_orbslam_loop_closer_library
  srrg_orbslam_tracker_library
  srrg_orbslam_correspondence_finder_library
  srrg_orbslam_dbow2_addons_library
  ${OpenCV_LIBS}
)
