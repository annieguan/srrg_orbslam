add_library(srrg_orbslam_solver_library SHARED
  PnPsolver.cc
  Sim3Solver.cc
  )

target_link_libraries(srrg_orbslam_solver_library
  srrg_orbslam_types_library
  ${OpenCV_LIBS}
  )
