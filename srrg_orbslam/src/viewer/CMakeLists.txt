add_library(srrg_orbslam_viewer_library SHARED
  FrameDrawer.cc
  MapDrawer.cc
  Viewer.cc
)

target_link_libraries(srrg_orbslam_viewer_library
  srrg_orbslam_types_library
  srrg_gl_helpers_library
  ${OpenCV_LIBS}
  ${Pangolin_LIBRARIES}
  )
