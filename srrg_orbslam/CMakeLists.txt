cmake_minimum_required(VERSION 2.8.3)
project(srrg_orbslam)

#ia cmake_modules
find_package(srrg_cmake_modules REQUIRED)
set(CMAKE_MODULE_PATH ${srrg_cmake_modules_INCLUDE_DIRS})

#ia compile flags
set(CMAKE_BUILD_TYPE RELEASE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -fPIC --std=c++11 -march=native")
message("${PROJECT_NAME}|build type: [ ${CMAKE_BUILD_TYPE} ]")
message("${PROJECT_NAME}|build flags: [${CMAKE_CXX_FLAGS} ]")

#ia find damn packages
find_package(Eigen3 REQUIRED)
find_package(G2O REQUIRED)

find_package(OpenCV REQUIRED)
message("${PROJECT_NAME}|found OpenCV version: '${OpenCV_VERSION}' (${OpenCV_DIR})")

find_package(DBoW2 REQUIRED)
message("${PROJECT_NAME}|found DBoW2: (${DBoW2_DIR})")

find_package(Pangolin REQUIRED)

#ia find the other damn required catkin packages 
find_package(catkin REQUIRED COMPONENTS
  srrg_core
  srrg_gl_helpers
)

#ia include damn directories
include_directories(${EIGEN3_INCLUDE_DIR}
  ${OpenCV_INCLUDE_DIRS}
  ${G2O_INCLUDE_DIR}
  ${DBoW2_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
  ${Pangolin_INCLUDE_DIRS}
  ${PROJECT_SOURCE_DIR}/src)
  
#ds help the catkin tool on 16.04 (cmake seems unable to find single libraries, although catkin claims the link_directories call is not required)
#ds in order to avoid linking against the catkin_LIBRARIES bulk everytime enable this so one can select single libraries
link_directories(${catkin_LIBRARY_DIRS})
  
#ia create a damn catkin package
catkin_package(
  INCLUDE_DIRS  src
  LIBRARIES
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  srrg_orbslam_utils_library
  srrg_orbslam_correspondence_finder_library
  srrg_orbslam_dbow2_addons_library
  srrg_orbslam_detector_library
  srrg_orbslam_local_mapper_library
  srrg_orbslam_loop_closer_library
  srrg_orbslam_optimizer_library
  srrg_orbslam_solver_library
  srrg_orbslam_tracker_library
  srrg_orbslam_types_library

  CATKIN_DEPENDS  srrg_core srrg_gl_helpers
)

add_subdirectory(src)
