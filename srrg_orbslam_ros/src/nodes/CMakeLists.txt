# Monocular Node
add_executable(monocular_node
  ros_mono.cc
)

target_link_libraries(monocular_node
  ${OpenCV_LIBS}
  ${EIGEN3_LIBS}
  ${Pangolin_LIBRARIES}
  ${DBoW2_LIBRARIES}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
)

# RGB-D Node
add_executable(rgbd_node
  ros_rgbd.cc
)

target_link_libraries(rgbd_node
  ${OpenCV_LIBS}
  ${EIGEN3_LIBS}
  ${Pangolin_LIBRARIES}
  ${DBoW2_LIBRARIES}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
)

# Stereo Node
add_executable(stereo_node
  ros_stereo.cc
)

target_link_libraries(stereo_node
  ${OpenCV_LIBS}
  ${EIGEN3_LIBS}
  ${Pangolin_LIBRARIES}
  ${DBoW2_LIBRARIES}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
)
