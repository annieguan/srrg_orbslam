# SRRG ORB-SLAM2

An up-to-date version of ORB-SLAM2. No embedded ThirdParty libraries (it uses the original g2o and DBoW2).

**Mantainers of this repo:** [Bartolomeo Della Corte](http://bartdc.gitlab.io/)

**ORB-SLAM Authors:** [Raul Mur-Artal](http://webdiis.unizar.es/~raulmur/), [Juan D. Tardos](http://webdiis.unizar.es/~jdtardos/), [J. M. M. Montiel](http://webdiis.unizar.es/~josemari/) and [Dorian Galvez-Lopez](http://doriangalvez.com/) ([DBoW2](https://github.com/dorian3d/DBoW2))

## Dependencies

* [DBoW2](https://github.com/dorian3d/DBoW2)
* [g2o](https://github.com/RainerKuemmerle/g2o)
* [Pangolin](https://github.com/stevenlovegrove/Pangolin)
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)

## Build me

* compile the three external libraries (DBoW2, g2o, Pangolin) following the respective instructions.
* create a catkin workspace and link inside `srrg_cmake_modules`, `srrg_core`, `srrg_gl_helpers` and `srrg_orbslam`
* `catkin build` / `catkin_make`

## Use me

* `rosrun srrg_orbslam <TAB> -h`
* `rosrun srrg_orbslam_ros <TAB> -h`

## License ##

ORB-SLAM2 is released under a [GPLv3 license](https://github.com/raulmur/ORB_SLAM2/blob/master/License-gpl.txt). For a list of all code/library dependencies (and associated licenses), please see [Dependencies.md](https://github.com/raulmur/ORB_SLAM2/blob/master/Dependencies.md).

If you use this software in an academic work, please cite the relevant [ORB-SLAM](https://github.com/raulmur/ORB_SLAM2) paper.

## TODO ##

- [x] Use Updated G2o/DBoW/Pangolin
- [x] txtio/boss apps
- [x] showing ground truth trajectory in txt/boss apps
- [ ] cv::Mat to Eigen for points and transforms
- [ ] massive cleaning